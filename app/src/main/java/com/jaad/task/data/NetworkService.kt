package com.jaad.task.data

import com.google.gson.JsonObject
import com.jaad.task.BuildConfig
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkService {

    @GET("latest")
    fun getLatest(
        @Query("access_key") key: String = BuildConfig.API_KEY ,
        @Query("symbols") symbol :String
    ): Single<JsonObject>

}