package com.jaad.task.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonObject
import com.jaad.task.BuildConfig
import com.jaad.task.data.ApiClient
import com.jaad.task.data.NetworkService
import com.jaad.task.databinding.ActivityMainBinding
import com.mynameismidori.currencypicker.CurrencyPicker
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val compositeDisposable = CompositeDisposable()
    var symbol: String = "EUR"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSelect.setOnClickListener {

            val picker = CurrencyPicker.newInstance("Select Currency")

            picker.setListener { name, code, symbol, flagDrawableResID ->
                binding.btnSelect.text = code
                this.symbol = code
                picker.dismiss()
            }
            picker.show(supportFragmentManager, "CURRENCY_PICKER")
        }

        binding.btnCalculate.setOnClickListener {
            getData(binding.etAmount.text.toString())
        }
    }

    @SuppressLint("SetTextI18n")
    private fun getData(amount: String) {

        val service = ApiClient.getClient().create(NetworkService::class.java)

        compositeDisposable.add(
            service.getLatest(BuildConfig.API_KEY, symbol)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    val data: JsonObject = response.get("rates") as JsonObject

                    val rate = data.get(symbol)

                    binding.tvResult.text = String.format(
                        Locale.US, "%.2f",
                        (rate.toString().toDouble() * amount.toDouble())
                    ) + "  " + symbol
                },
                    { t ->
                        Toast.makeText(this, t.message, Toast.LENGTH_SHORT).show()
                    })
        )
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}




